const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/playground')
    .then(()=>console.log('Connected to databse successfully...'))
    .catch((err)=>console.log(`Couldnt connect to mongodb:${err}`));

const courseSchema = new mongoose.Schema({
    name:String,
    author:String,
    tags:[String],
    date: { type:Date, default:Date.now },
    isPublished: Boolean
});

const Course = mongoose.model('Course', courseSchema);
const course =  new Course({
     name: 'Angular course',
     author: 'Mosh',
     tags: ['ngular','frontend'],
     isPublished:true
});

async function createCourse(){
    
const result = await course.save();
console.log(result); 
}

//createCourse();

/* Mongodb Comparison Operators
    eq (equal)
    ne (not equal)
    gt (greater than)
    gte(greater than or equal to)
    lt (less than)
    lte(less than or equal to)
    in ()
    nin(not in)
*/

/* Mongodb Logical Opeartors
    or
    and
*/

async function getCourses(){
   
    const pageNumber = 2;
    const pageSize = 10;
    // /api/courses?pageNumber=2&pageSize=10
    
    const courses = await Course
        // .find({author:'Mosh',isPublished:true})
        
         /* Implementing greater than operator in mongo query */
            //.find({price: { $gte: 10 ,$lte: 20 } })

        /* Implementing logical opearators */
            //.find()
            //.or([ { author: 'Mosh' } , { isPublished: true } ])
            //.and([])

        /* regular expression Courses starts with mosh */
            //.find({ author: /^Mosh/ }) /* ^ sign in regular expression indicates starts of the string */

        /* Ends with hamedani */
           // .find({author: /Hamedani$/i}) /* $ sign in regular expression indicates end of the string ,appends to make thsi query case insensitive */

        /* author conatains Mosh either in beginning,middle,end */
           // .find({author: /.*Mosh.*/i})

        /* Implementing Pagination */   
        //const pageNumber = 2;
        //const pageSize = 10;
        /* /api/courses?pageNumber=2&pageSize=10 */ 
        
        .find({ author:'Mosh',isPublished:true})
        .skip((pageNumber - 1) * pageSize)
        .limit(pageSize)
        .sort({name: -1})
        .count();
        //.select({name:1,tags:1});
    console.log(courses);
}
getCourses();

/* Update course */
async function updateCourse(id){
    /* Normal Approach:Query First
        findbyId()
        Modify its property
        save()
    */
   /* Other Approach: Update First
        Update directly
        Optionally :get the udpated document
   */

   /* Implementing Query first approach */
  const course = await Course.findById(id);
  if(!course) return
  course.isPublished = true;
  course.author = 'Another author'
  const result = await course.save();
  console.log(result);

}

async function updateCourse2(id){
  const result2 = await Course.findByIdAndUpdate(id,{
    $set: {
        author:'Jalaj',
        isPublished: false
    }
} ,{ new : true});
console.log(result2);
}

updateCourse('5d8a71eeaecd9a1f5c807e57');
updateCourse2('5d8a7160220fcf20a881d67f');

async function removeCourse(id){
    // const result = await Course.deleteOne({_id:id}); /* deleteMany  or findByIdAndRemove */
    const result = await Course.findByIdAndRemove(id); /* deleteMany  or findByIdAndRemove */
    console.log(result);
}
removeCourse('5d8a7160220fcf20a881d67f');   