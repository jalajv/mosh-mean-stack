const p = new Promise((resolve,reject)=>{
 setTimeout(() => {
   // resolve(1);    
    reject(new Error('message'));
 }, 2000);
 
})

p
.then(result=> console.log(result))
.catch(err=> console.log('Error:',err.message));

/* Promise chaining */
const p1 = new Promise((resolve)=>{
    setTimeout(() => {
        console.log('Async Operation1...');
        resolve(1);
       //reject('Promise p1 is rejected');
    }, 2000);
})

const p2 = new Promise((resolve)=>{
    setTimeout(() => {
        console.log('Aync Operaion2...');
        resolve(2);
    }, 2000);
})

/* if any of promise is rejected then the all Promise will also be rejected */
Promise.all([p1,p2])
    .then(result =>console.log(result))
    .catch(err=>console.log('Error',err.message));

 /* As soon as the first promise is resoved it will get end */   
Promise.race([p1,p2])
    .then(result=>console.log(result));