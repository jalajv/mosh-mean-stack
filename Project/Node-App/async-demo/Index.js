console.log('Before');
/* Old way of calling asynchronous calls or Callback Hell or Christmas tress proble */
// getUser(1,(user)=>{
//     console.log(`user: ${user}`);
//     getRepositories((repos)=>{
//         console.log('Repos:-'+ repos);
//     })
// });


/* solution to callback Hell problem Implementing Promises */
// getUser(1)
//     .then(user=>{ getRepositories(user.gitHubUserName)})
//     .then(repos=>getCommits(repos))
//     .then(commits=>console.log(commits))
//     .catch(err=>console.log("Error:"+ err.message));

/* Async Await Approach
 is used to call aynchrouns calls synchronously */
async function displayCommits(){
   try{
        const user = await getUser(1);
        const repos = await getRepositories(user.gitHubUserName);
        const commits= await getCommits(repos);
        console.log(commits);   
   }
   catch(err){
       console.log('Error',err);
   }
}
 displayCommits();

console.log('After');

/* Callback
   Promises
   Async/await
*/

function getUser(id){
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
          //  resolve([{ id:id, gitHubUserName:'jalajv'}]);
          reject('could not get users');
        },2000);
    });
    
}

function getRepositories(username){
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
            resolve(['repo1','repo2','repo3']);
        }, 2000);   
    });
}

function getCommits(repo){
    return new Promise((resolve,reject)=>{
        setTimeout(() => {
                resolve([{file1:"Index.js",file2:"promise.js"}]);
        }, 2000);
    })
}