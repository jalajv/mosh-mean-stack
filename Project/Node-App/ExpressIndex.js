const Joi     = require('joi');
const logger  = require('./logger'); 
const express = require('express');
const morgan  = require('morgan');

const startupDebugger = require('debug')('app:startup');
const dbDebugger = require('debug')('app:db');
const config  = require('config');
const app     = express();

const coursesRouter = require('./routes/coursesRoute');

/* Start: Creating View Engine to return dynamic html to client */
    app.set('view engine', 'pug');
    app.set('views','./views'); /* Optional: Defaul path to all the views i.e in views folder at root */
    app.get('/api/html/',(req,res)=>{
        res.render('index',{title:'My express app',message:'Hello world:API is returning dynamic html using pug'});
    });
/* End: Creating View Engine to return dynamic html to client */


/* Start: Implementing morgan , debugger etc libraries */
var devenv = config.util.getEnv('NODE_ENV');
console.log(devenv);
if(devenv === 'development'){
    /* morgan is also a middle ware function this is used to log http request*/
    app.use(morgan('tiny'));
    console.log('Morgan enabled');
    startupDebugger('Morgan enabled');
}
else if(devenv === 'production'){
   // console.log('production value set to enviroment variable');
}
else
{
   // console.log('enviroment variable value is not setted');
}
/* End: Implementing morgan , debugger etc libraries */


/* configuration  */
console.log(`Application Name: ${config.get('name')}`);
console.log(`Mail server name: ${config.get('mail.host')}`);
//console.log(`Mail Password: ${config.get('password')}`);


/* setting enviroment variables */
// console.log(`NODE_ENV: ${process.env.NODE_ENV}`);
// console.log(`app: ${app.get('env')}`);           /* returns development by default */

/* built in middleware function */
app.use(express.json());

/* Another built in middleware function */
app.use(express.urlencoded({extended:true})); //key=value&key=value

/* last middleware function of express */
app.use(express.static('public'));  /* public is a argument to function and thats the name of the folder */



/* Db work... */
dbDebugger('Connected to the database');
/* call debugger first set enviroment variable like set DEBUG=app:startup,app:db then node expressindex.js */

/* Custome middleware function */
//app.use(logger);

/* Custome middleware function */
app.use((req,res,next)=>{
    console.log('logging...');
    next();
});

/* All the APIs are created here */
    app.use('/api/courses',coursesRouter);
/* End All the APIs are created here */


app.listen(3000,()=>{
    console.log('Listening on Port 3000...');
});