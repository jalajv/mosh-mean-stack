const express = require('express');
const router = express.Router();


/* All the APIs are created here */


const courses=[
    {id:1,name:'Course1'},
    {id:2,name:'Course2'},
    {id:3,name:'course2'}
]

/* Get api */
router.get('/', (req,res)=>{
    res.send('Hello World');
});

/* Get api which returns an array */
router.get('/',(req,res)=>{
    res.send([1,2,3,4,5]);
})

/* Get api which accecpting id in url and returning that paramatere */
router.get('/:id',(req,res)=>{
    res.send(req.params.id);
});

/* get api which accecpts multiple parameters in URL */ 
router.get('/:year/:month',(req,res)=>{
    res.send(req.params);
});

/* get api which is sending values in query string */
router.get('/',(req,res)=>{
    res.send(req.query);
});

router.get('/:id',(req,res)=>{
    let course=courses.find(c=>c.id===parseInt(req.params.id));
    if(!course){
        res.status(404).send('The course with the given id is not found');
    }
    else
    {
        res.send(course);
    }
});

/* HTTP Post API to save data */
router.post('/',(req,res)=>{
    const {error}=validateCourse(req.body); // result.error this is called object destructing
    //if(result.error) 
    if(error) 
    {
        return res.status(400).send(result.error.details[0].message);
    }

    const course=
    {
        id:courses.length+1,
        name:req.body.name
    }
    courses.push(course);
    res.send(course);
});

/* Http PUT api to update course */
router.put('/:id',(req,res)=>{
    /* Lookup the course
    If not exists ,return 404 
    Validate the course
    If Invalid ,return 400 -Bad Requests */

    //Update the course and return the updated course to client.
    const course=courses.find(c=>c.id===parseInt(req.params.id));
    if(!course) return res.status(404).send('The course with the given id was not found');

   const result=validateCourse(req.body);
   const {error}=validateCourse(req.body); // result.error this is called object destructing
    //if(result.error) 
    if(error) 
    {
        return res.status(400).send(result.error.details[0].message);
        
    }

    course.name=req.body.name;
    res.send(course);

});

/* Http Delete API to delete  a specific course */
router.delete('/:id',(req,res)=>{
    //Look up the course
    //Not exists 404 error

    //Delete

    const course=courses.find(c=>c.id===parseInt(req.params.id));
    if(!course) {
       return res.status(404).send('The course with the given id is not found');
    }
    
    //Delete
    const index=courses.indexOf(course);
    courses.splice(index,1);
    res.send(course);

});

//Input Validation via  Joi
function validateCourse(course)
{
    const schema={
        name:Joi.string().min(3).required()
    }
    return Joi.validate(course,schema);
}

module.exports = router;