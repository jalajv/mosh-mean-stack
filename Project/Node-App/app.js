//---http
//--------------------------------------------------------------------------------------------------------------------
    const http=require('http');
    const server=http.createServer(function(req,res){
        if(req.url==='/'){
            res.write('Hello world');
            res.end();
        }
        else
        {
            res.write('error while creating server');
        }
    });

  
    server.listen(3000);
    console.log('listening... on port 3000');

//--------------------------------------------------------------------------------------------------------------------
//--End http

//---Calling Events from separate event class in EventClass.js file
//--------------------------------------------------------------------------------------------------------------------
    const eventClass=require('./EventClass');
    const objEventClass=new eventClass();
    objEventClass.on('messagelogged',function(){
        //console.log('Implementing a function in main class which is called in diffrent class i.e EventClass')
    });

    objEventClass.log('Calling funtion log')
//--------------------------------------------------------------------------------------------------------------------
//---End


//---Events
//--------------------------------------------------------------------------------------------------------------------
    const EventEmitter= require('events');
    const emitter=new EventEmitter();
    
      //Register a listener or Created an event
      emitter.on('messageLoggedEvent',(arg)=>{  
        //console.log('messageLoggedEvent event called',arg);
        //console.log('--------------------------------');
    });

    emitter.on('click',function(arg){
        //console.log('--------------------------------');
        //console.log('on click event called ',arg)
    })
    //Raise an event or calling an event
    emitter.emit('messageLoggedEvent',{id:1,Name:'Jalaj'});
    emitter.emit('messageLoggedEvent',{id:2,Name:'Sandeep'});
    emitter.emit('click',{id:1,onclick:'clicked'});
    //emite means making a noise 

  

//--------------------------------------------------------------------------------------------------------------------
//---Events End

//---File System
//--------------------------------------------------------------------------------------------------------------------
const fs=require('fs');
const files=fs.readdirSync('./');
console.log('--------------------------------');
//console.log(files);
console.log('--------------------------------');
fs.readdir('./',function(err,files){
    if(err){
        console.log('Error: '+err);
    }
    else{
      //  console.log('Result',files);
    }
});
//--------------------------------------------------------------------------------------------------------------------
//---FS End


//---OS
//--------------------------------------------------------------------------------------------------------------------
    const OS =require('os');
    var totalmem= OS.totalmem();
    var freemem=OS.freemem();

//console.log(totalmem);
//console.log(freemem);

//Template String example
//ES6

//console.log(`Total memory in OS: ${totalmem}`);
//console.log(`Total Free memory in OS:${freemem}`)

//so in the example of template string feature of ES6 we dont have to conactenate the strings we can directly call variables with using ${}

//--------------------------------------------------------------------------------------------------------------------
//---OS End

//---using path
//--------------------------------------------------------------------------------------------------------------------
// Path object have diffrent userful properties like dir to get directory of file and base,name etc
const path=require('path');
var pathObj= path.parse(__filename);
//console.log(pathObj);
//--------------------------------------------------------------------------------------------------------------------
//---End

//---Example of calling values,functions from diffrent files like logger.js here app.js file acts like a main file
//--------------------------------------------------------------------------------------------------------------------
// const logger= require('./logger'); //here variable type is const because no one can override the value of const variable
// console.log(logger)
//logger.log('Here main app.js is calling a function from logger.js module.Its an example of calling from diffrent file')
//--------------------------------------------------------------------------------------------------------------------
//console.log('calling variable value from logger file the value is='+logger.endPointurl);

//---End

function sayHello(name){
    console.log('Hello '+name);
}
//sayHello('world');
